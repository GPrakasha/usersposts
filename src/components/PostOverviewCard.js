import React,{useState} from 'react';
import '../styles/post.css';
import {Card, Button, Container} from 'react-bootstrap';
import {getComments, deletePost} from '../services/apiEndPoint';
import Comment from '../components/Comment';
import imagePlaceholder from '../imageP.png';
import {useHistory} from 'react-router-dom';
import ErrorScreen from './ErrorScreen';

function PostOverviewCard(props) {

    const [comments, setComments] = useState([]);
    const [showComments, setShowComments] = useState(false);
    const history = useHistory();
    const [loadingDelete, setLoadingDelete] = useState(false);
    const [loadingComments, setLoadingComments] = useState(false);
    const [showError, setshowError] = useState(false);
    console.log(history);

    function handleDeletePost(PostId) {
        setLoadingDelete(true);
        deletePost(PostId)
        .then((res) => {
            console.log(res.status)
            if(res.status === 200) {
                setLoadingDelete(false);
                history.push(`/userId/${history.location.state.userId}`);
            } else {
                setshowError(true);
                setLoadingDelete(false);
            }
        }).catch((err) => {
            setshowError(true);
            setLoadingDelete(false);
        })
    }

    function handleViewComments() {
        if(!comments.length > 0) {
            setLoadingComments(true);
            getComments(props.PostId)
            .then((res) => res.json())
            .then((res) => {
                if(res) {
                    setComments(res);
                    console.log(res);
                } else {
                    console.log("response from server", res);
                    setshowError(true);
                }
                setLoadingComments(false);
            })
            .catch((err) => {
                console.log(err);
                setshowError(true);
                setLoadingComments(false);
            });
        }
        setShowComments(!showComments);
    }

    return (
        <Card className="mt-4 cardStyleInOverview">
            <Card.Img variant="top" src={imagePlaceholder} />
            <Card.Header className="mb-0" as="h5">{props.postDetails.title}</Card.Header>
            <Card.Body>
                <Card.Text>
                    {props.postDetails.body}
                </Card.Text>
                <div className="d-flex flex-row border-bottom">
                    <footer className="blockquote-footer mb-3 ml-auto">
                        <cite title="Source Title">
                            {JSON.parse(localStorage.getItem("user")).name}
                        </cite>
                    </footer>
                </div>
                <div className="d-flex flex-row mt-3">
                    <div
                        onClick={handleViewComments}
                        className="text-secondary mr-auto mt-auto mb-auto d-flex align-items-center"
                        style={{ cursor: "pointer" }}
                    >
                        {
                            loadingComments ?
                                <div className="spinner-grow text-primary mr-2" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                            :
                                null
                        }
                        {showComments ? "hide comments" : "view comments"}
                    </div>
                    <Button onClick={() => handleDeletePost(props.PostId)}>
                        {
                            loadingDelete ? 
                                <span 
                                    className="spinner-border spinner-border-sm mr-2" 
                                    role="status" 
                                    aria-hidden="true"
                                >                                    
                                </span> 
                            : 
                                null
                        }
                        Delete
                    </Button>
                </div>
                {
                    showComments && !showError ?
                        <Container className="mt-3">
                            {
                                comments.map((comment, index) => (
                                    <Comment comment={comment} key={index} />
                                ))
                            }
                        </Container>
                        : showError ?
                            <div className="d-flex mt-3">
                                <ErrorScreen></ErrorScreen>
                            </div>
                        :   
                            null
                }
            </Card.Body>
        </Card>
    );
}

export default PostOverviewCard;