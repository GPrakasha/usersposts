import React from 'react';

function Loader () {
    return (
        <div 
            className="spinner-grow text-primary m-auto" 
            style={{width: "50px", height: "50px"}} 
            role="status"
        >
            <span className="sr-only">Loading...</span>
        </div>
    );
}

export default Loader;