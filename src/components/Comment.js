import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import '../styles/comment.css'

function Comment(props) {
    return (
        <Row className="pt-3 pb-3">
            <Col xs={"auto"} className="pr-0">
                <div className="letterIcon m-auto">
                    {props.comment.name.split("")[0].toUpperCase()}
                </div>
            </Col>
            <Col >
                <div className="p-3 mb-2 bg-light text-dark">
                    <Button className="text-dark">
                        {props.comment.name}
                    </Button>
                    <p className="text-secondary mb-0 mt-2">
                        {props.comment.body}
                    </p>
                </div>
            </Col>
        </Row>
    );
}

export default Comment;