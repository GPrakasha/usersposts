import React from 'react';

function ErrorScreen() {
    return(
        <div className="alert alert-danger m-auto" role="alert">
            We are facing some issue in providing the required details, we are working on it and will get it fixed soon.
        </div>
    );
}

export default ErrorScreen;