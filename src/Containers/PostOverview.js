import React,{useState, useEffect} from 'react';
import {getPostDetails} from '../services/apiEndPoint';
import PostOverviewCard from '../components/PostOverviewCard';
import Loader from '../components/Loader';
import ErrorScreen from '../components/ErrorScreen';

function PostOverview() {
    
    const PostId = window.location.pathname.split("/")[2];
    const [loaded, setLoaded] = useState(false);
    const [showError, setShowError] = useState(false);
    const [postDetails, setPostDetails] = useState({});
 
    useEffect(() => {
        getPostDetails(PostId)
        .then((response) => response.json())
        .then((res) => {
            if(res) {
                setPostDetails(res);
                setLoaded(true);
            } else {
                console.log("Response from server", res);
                setShowError(true);
            }
        })
        .catch((err) => {
            console.log(err);
            setShowError(true);
        });
         // eslint-disable-next-line react-hooks/exhaustive-deps
    },[]);

    return (
        <div className="pt-4">
            {
                loaded ?
                    <PostOverviewCard PostId={PostId} postDetails={postDetails} />
            : (showError) ?
                <div className="vh-100 d-flex">
                    <ErrorScreen></ErrorScreen>
                </div>
            :
                <div className="vh-100 d-flex">
                    <Loader></Loader>
                </div>
            }
        </div>
    );

}

export default PostOverview;