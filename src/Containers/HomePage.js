import React from 'react';
import '../styles/homepage.css';
import {getUsers} from '../services/apiEndPoint.js';
import { Table, Button, FormControl, InputGroup } from 'react-bootstrap';
import Loader from '../components/Loader';
import ErrorScreen from '../components/ErrorScreen';

class HomePage extends React.Component {

    constructor() {
        super();
        this.state = {
            loaded: false,
            FilteredTable: [],
            showErrorState: false,
        };
        this.userTable = [];
        this.filterTableByColumn = this.filterTableByColumn.bind(this);
        this.handleViewPosts = this.handleViewPosts.bind(this);
    }

    componentDidMount() {
        getUsers()
            .then(response => response.json())
            .then((res) => {
                if (res) {
                    this.userTable = res;
                    console.log(this.userTable);
                    this.setState({
                        loaded: true,
                        FilteredTable: this.userTable
                    })
                } else {
                    console.log("got this as response", res);
                    this.setState({showErrorState: true, loaded: false});
                }
            }).catch((err) => {
                console.log(err);
                this.setState({showErrorState: true, loaded: false});
            });
    }

    filterTableByColumn(columnName, searchQuery) {
        let filteredData = this.userTable.filter(function (e) {
            searchQuery = searchQuery.toLocaleLowerCase();
            return (columnName === "name") ? e[columnName].toLocaleLowerCase().includes(searchQuery) : e[columnName].name.toLocaleLowerCase().includes(searchQuery);
        });
        this.setState({FilteredTable: filteredData});
    }
    
    handleViewPosts(user) {
        localStorage.setItem("user", JSON.stringify(user));
        window.location.href = `/userId/${user.id}`;
    }

    render() {
        return (
            this.state.loaded ?
                <Table className="tableCustom" bordered hover responsive>
                    <thead>
                        <tr>
                            <th>
                                <div className="columnHead">
                                    Name
                                    <InputGroup className="searchButton mt-2">
                                        <FormControl
                                            placeholder="Search by Name"
                                            aria-label="name"
                                            aria-describedby="basic-addon1"
                                            name="name"
                                            onChange={
                                                (event) => this.filterTableByColumn(event.target.name, event.target.value)
                                            }
                                        />
                                    </InputGroup>
                                </div>
                            </th>
                            <th>
                                <div className="columnHead">
                                    Company
                                    <InputGroup className="searchButton mt-2">
                                        <FormControl
                                            placeholder="Search by Name"
                                            aria-label="name"
                                            aria-describedby="basic-addon1"
                                            name="company"
                                            onChange={
                                                (event) => this.filterTableByColumn(event.target.name, event.target.value)
                                            }
                                        />
                                    </InputGroup>
                                </div>
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.loaded &&
                                this.state.FilteredTable.map((row, index) => (
                                    <tr key={index}>
                                        <td className="tableData">{row.name}</td>
                                        <td className="tableData">{row.company.name}</td>
                                        <td className="tableData">
                                            <div onClick={() => this.handleViewPosts(row)} className="d-flex">
                                                <Button variant="outline-primary" className="m-auto">
                                                    View Posts
                                                </Button>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                    }
                    </tbody>
                </Table>
            :  (this.state.showErrorState) ?
                    <div className="vh-100 d-flex">
                        <ErrorScreen></ErrorScreen>
                    </div>
            :
                <div className="vh-100 d-flex">
                    <Loader></Loader>
                </div>
        );
    }
}

export default HomePage;