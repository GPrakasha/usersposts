import React,{useEffect, useState} from 'react';
import {getPosts} from '../services/apiEndPoint.js';
import {Card, InputGroup,FormControl} from 'react-bootstrap';
import '../styles/post.css';
import {useHistory} from 'react-router-dom';
import imagePlaceholder from '../imageP.png';
import Loader from '../components/Loader.js';
import ErrorScreen from '../components/ErrorScreen';

function UsersPosts() {
    const id = window.location.pathname.split("/")[2];
    const [posts, setPost] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const history = useHistory();
    const [filtered, setFiltered] = useState([]);
    const [searchQuery, setSearchQuery] = useState("");
    const [showError, setShowError] = useState(false);

    useEffect(() => {
        getPosts(id)
        .then((response) => response.json())
        .then((resp) => {
            console.log(Object.entries(resp).length)
            if(Object.entries(resp).length) {
                setPost(resp);
                setFiltered(resp);
                setLoaded(true);
                return;
            } else {
                setShowError(true);
                setLoaded(true);
                console.log("response from server",resp);
            }
        })
        .catch((err) => {
            console.log("response from server", err)
            setShowError(true);
            setLoaded(true);
        });
         // eslint-disable-next-line react-hooks/exhaustive-deps
    },[]);

    useEffect(() => {
        let filteredData = posts.filter(function (post) {
            return post.title.toLocaleLowerCase().includes(searchQuery.toLocaleLowerCase());
        });
        setFiltered(filteredData);
         // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchQuery])
    
    return ( 
        <div className="m-auto">
             <InputGroup className="mb-4 w-50 ml-auto mr-auto" style={{marginTop: "4%"}}>
                <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-default">Search posts</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    aria-label="Default"
                    aria-describedby="inputGroup-sizing-default"
                    placeholder="posts title"
                    onChange={(event) => setSearchQuery(event.target.value)}
                />
            </InputGroup>
            {
                loaded && !showError ?
                    <div className="d-flex flex-wrap mt-4 ml-auto mr-auto mb-auto postContainerWidth">
                        {
                            loaded &&
                            filtered?.map((item, index) => (
                                <Card
                                    key={index}
                                    className="cardStyle"
                                    onClick={() => history.push(`/posts/${item.id}`, { userId: id, postId: item.id })}
                                >
                                    <Card.Img className="postsImaage" variant="top" src={imagePlaceholder} />
                                    <Card.Header className="mb-0" as="h5">{item.title}</Card.Header>
                                </Card>
                            ))
                        }
                    </div>
                : showError ?
                        <div className="d-flex vh-100">
                            <ErrorScreen></ErrorScreen>
                        </div>
                :
                    <div className="vh-100 d-flex">
                        <Loader></Loader>
                    </div>
            }
        </div>
    );
}

export default UsersPosts;