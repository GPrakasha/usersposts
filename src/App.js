import HomePage from './Containers/HomePage';
import UsersPosts from './Containers/Posts';
import PostOverview from './Containers/PostOverview';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route exact path="/">
					<HomePage/>
				</Route>
				<Route exact path="/userId/:id">
					<UsersPosts/>
				</Route>
				<Route exact path="/posts/:id">
					 {(props) => (<PostOverview {...props}/>)}
				</Route>
			</Switch>
		</BrowserRouter>
	);
}

export default App;
