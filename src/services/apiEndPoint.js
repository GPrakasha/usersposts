export async function getUsers() {
    const response = await fetch('https://jsonplaceholder.typicode.com/users');
    return response; 
}

export async function getPosts(userId) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts?userId=${userId}&skip=0&limit=10`);
    return response;
}

export async function getPostDetails(postId) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`);
    return response;
}

export async function getComments(postId) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
    return response;
}

export async function deletePost(postId) {
    const config = {
        method: "Delete"
    }
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, config);
    return response;
}